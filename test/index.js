const tape = require('tape')
const Vertex = require('../')

tape('basic', t => {
  let root = new Vertex()
  const path = ['one', 'two', 'three']
  const value = 'this is a leaf'
  const leaf = new Vertex(value)

  t.equals(leaf.isLeaf, true, 'should be a leaf')

  let v3 = new Vertex(value)
  root.set(path[0], v3)
  t.equals(root.edges.size, 1, 'the root should have one edge')
  root.set(path, leaf)

  const leaf2 = root.get(path)
  t.equals(leaf2, leaf, 'set and get should work')

  const leafRoot = leaf2.root
  t.equals(leafRoot, root, 'should return the root')

  v3 = root.get(path[0])

  t.equals(v3.value, value, 'set and get should work for single path')

  const nodes = [...root]
  t.equals(nodes.length, 4, 'should iterate through the nodesjs')

  let pathNodes = [...root.walkPath(path)]
  t.equals(pathNodes.length, 4, 'path length should be 4')

  root.del(path)
  pathNodes = [...root.walkPath(path)]
  t.equals(pathNodes.length, 2, 'delete should work')

  root.del(path[0])
  t.equals(root.isEmpty, true, 'delete should work')

  t.equals(root.del(path), false, 'should not delete non-existant paths')

  root.set(path, leaf)
  root.set(path[0], leaf)
  pathNodes = [...root.walkPath(path)]
  t.equals(pathNodes.length, 2, 'it should overwrite the previous vertex')

  t.end()
})

tape('copy', t => {
  let root = new Vertex()
  const path = ['one', 'two', 'three', 'four']
  const value = 'this is a leaf'
  root.set(path, new Vertex(value))
  let copy = root.copy()

  t.equals(copy.get(path), root.get(path), 'copy should intial have the same vertex')
  path.push('five')
  copy.set(path, new Vertex('test'))
  path.pop()

  path.push('five and a half')
  copy.set(path, new Vertex('test'))
  path.pop()

  t.notEqual(copy.get(path), root.get(path), 'shoudlnt have the same vertex')

  copy = root.copy()
  copy.set(path.slice(0, 2).concat(['two and a half']), new Vertex('new vertex'))
  copy.del(path)

  t.notEqual(copy.get(path.slice(0, 2)), root.get(path.slice(0, 2)), 'shoudlnt have the same vertex')

  copy = root.copy()
  root.set(path, new Vertex('another test'))

  t.notEqual(root.get(path), copy.get(path))

  t.end()
})

tape('root', t => {
  let root = new Vertex()
  const path = ['one', 'two', 'three', 'four']
  const value = 'this is a leaf'
  const leaf = new Vertex(value)
  root.set(path, leaf)
  root.del(path.slice(0, 2))
  t.notEquals(leaf.root, root)
  t.end()
})

tape('update self', t => {
  let root = new Vertex()
  const path = []
  const value = 'this is the value'

  root.set(path, new Vertex(value))
  const leaf = root.get(path)
  t.equals(root, leaf, 'root and leaf should be the same')
  t.equals(root.value, value)
  t.end()
})
