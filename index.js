/**
 * An Imperitave trie implemention
 */
module.exports = class Vertex {
  /**
   * Create a new vertex
   * @param {*} value the value that the vertex holds
   * @param {Map} edges the edges that this vertex has stored a `Map` edge name => `Vertex`
   */
  constructor (value, edges = new Map()) {
    this.value = value
    this.edges = edges
  }

  /**
   * @property {Vertex} returns the root vertex
   */
  get root () {
    let vertex = this
    while (vertex.parent) {
      vertex = vertex.parent
    }
    return vertex
  }

  /**
   * @property {boolean} Returns truthy on whether the vertexs is empty
   */
  get isEmpty () {
    return !this.edges.size && (this.value === undefined || this.value === null)
  }

  /**
   * @property {boolean} isLeaf wether or not the current vertex is a leaf
   */
  get isLeaf () {
    return this.edges.size === 0
  }

  /**
   * Updates an edge on a given path . If the path does not already exist this
   * will extend the path. If no value is returned then the vertex that did exist will be deleted
   * @param {Array} path
   * @param {Funtion} onVertex The update function which runs once the vertex is found.
   * it is given the found vertex and is suppose to return the vertex that will
   * be saved
   * @example
   * rootVertex.update(['some', 'path'], vertex => {
   *   vertex.value = 'updated value'
   *   return vertex
   * })
   */
  update (path, onVertex) {
    this.updateAsync(path, (foundVertex, update) => {
      update(onVertex(foundVertex))
    })
  }

  /**
   * just like `update` but async.
   * @param {Array} path
   * @param {Funtion} onVertex The update function which runs once the vertex is found.
   * it is given the found vertex and callback function which is called with value
   * that is to be saved
   * @example
   * rootVertex.update(['some', 'path'], ([vertex, cb]) => {
   *   vertex.value = 'updated value'
   *   cb(vertex)
   * })
   */
  updateAsync (path, onVertex) {
    path = formatPath(path)
    const pathCopy = path.slice(0)
    let vertex = this
    let edge

    while (path.length) {
      edge = path.shift()
      let nextVertex = vertex.edges.get(edge)
      if (!nextVertex) {
        nextVertex = new this.constructor()
      } else if (nextVertex.parent !== vertex) {
        nextVertex = nextVertex.copy()
      }
      nextVertex.parent = vertex
      vertex.edges.set(edge, nextVertex)
      vertex = nextVertex
    }

    onVertex(vertex, updatedVertex => {
      let parent = vertex.parent
      if (!pathCopy.length) {
        // update self
        vertex.edges = updatedVertex.edges
        vertex.value = updatedVertex.value
      }

      if (updatedVertex && parent) {
        updatedVertex.parent = parent
        parent.edges.set(edge, updatedVertex)
      } else {
        // deletes vertex
        while (pathCopy.length) {
          edge = pathCopy.pop()
          parent.edges.delete(edge)
          if (parent.isEmpty) {
            const nextParent = parent.parent
            // remove the refernce to the parent
            delete parent.parent
            parent = nextParent
          } else {
            break
          }
        }
      }
    })
  }

  /**
   * Set an edge on a given path to the given the vertex
   * @param {Array} path
   * @param {Vertex} vertex
   */
  set (path, newVertex) {
    this.update(path, () => {
      return newVertex
    })
  }

  /**
   * get a vertex given a path
   * @param {Array} path
   * @return {Vertex}
   */
  get (path) {
    path = formatPath(path)
    let result = this
    while (path.length && result) {
      const edge = path.shift()
      result = result.edges.get(edge)
    }
    return result
  }

  /**
   * deletes an Edge at a given path
   * @param {Array} path
   * @return {Boolean} Whether or not anything was deleted
   */
  del (path) {
    let deleted
    this.update(path, foundVertex => {
      deleted = !foundVertex.isEmpty
    })
    return deleted
  }

  /**
   * Creates an copy of the current trie
   * @return {Vertex}
   */
  copy () {
    const copy = new this.constructor()
    const copy2 = new this.constructor()
    copy.value = copy2.value = this.value
    copy.edges = new Map(this.edges)
    copy2.edges = new Map(this.edges)
    for (const [, edge] of this.edges) {
      edge.parent = copy2
    }
    return copy
  }

  /**
   * Does a depth first iteration of the trie
   */
  * [Symbol.iterator ] () {
    for (const vertex of this.edges) {
      yield * vertex[1]
    }
    yield this
  }

  /**
   * walks along a path. Implemented as an itorator that returns
   * `[edge name, vertex]` on each found edge.
   */
  * walkPath (path) {
    path = formatPath(path)
    let vertex = this
    let edge
    while (vertex) {
      yield [edge, vertex]
      edge = path.shift()
      vertex = vertex.edges.get(edge)
    }
  }
}

function formatPath (path) {
  if (Array.isArray(path)) {
    return path.slice()
  } else {
    return [path]
  }
}
